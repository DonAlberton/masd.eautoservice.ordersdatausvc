﻿using Masd.EAutoService.OrdersDataUSvc.Model;

namespace Masd.EAutoService.OrdersDataUSvc.Logic
{
    public class OrderData : IOrderData
    {
        private static readonly object orderLock = new object();
        private static Order[] orders;
        private static string fileName = "orders.json";

        static OrderData()
        {
            var orderReader = new OrderDataFileHandler();
            lock (orderLock)
            {
                Order[] orders = OrderDataFileHandler.ReadOrders(fileName);
                OrderData.orders = orders;
            }
        }



        public Order[] GetOrders()
        {
            lock (orderLock)
            {
                return orders;
            }
        }

        public Order GetOrder(int id)
        {
            lock (orderLock)
            {
                return orders.FirstOrDefault(m => m.Id == id);
            }

        }

        public int GetOrderId()
        {
            lock (orderLock)
            {
                return orders.Last().Id;
            }

        }


        public void AddOrder(int mechanicId, int customerId, List<int> servicesList)
        {
            lock (orderLock)
            {
                Order order = new Order(orders.Last().Id + 1, mechanicId, customerId, servicesList, false);

                List<Order> ordersList = new List<Order>(orders);

                ordersList.Add(order);

                Order[] Or = ordersList.ToArray();

                OrderDataFileHandler.WriteOrders(Or, fileName);

                orders = Or;
            }
        }

        public void EditOrder(int id, bool isFinished)
        {
            lock (orderLock)
            {
                orders.FirstOrDefault(m => m.Id == id).IsFinished = isFinished;

                OrderDataFileHandler.WriteOrders(orders, fileName);
            }
            
        }

        public void AddServicesToOrder(int id, List<int> services)
        {
            lock (orderLock)
            {
                HashSet<int> existingServiceList = new HashSet<int>(orders.FirstOrDefault(m => m.Id == id).ServiceList);

                existingServiceList.UnionWith(new HashSet<int>(services));

                orders.FirstOrDefault(m => m.Id == id).ServiceList = existingServiceList.ToList();

                OrderDataFileHandler.WriteOrders(orders, fileName);
            }
        }
    }
}