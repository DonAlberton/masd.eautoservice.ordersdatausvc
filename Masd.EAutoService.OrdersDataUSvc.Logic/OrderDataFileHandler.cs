﻿using System.Text.Json;
using Masd.EAutoService.OrdersDataUSvc.Model;
using Masd.EAutoService.OrdersDataUSvc.Rest.Model;

namespace Masd.EAutoService.OrdersDataUSvc.Logic
{
    public class OrderDataFileHandler
    {
        public static OrderDTO[] ReadOrdersData(string fileName)
        {
            OrderDTO[] ordersDTO = JsonSerializer.Deserialize<OrderDTO[]>(File.ReadAllText(fileName));

            return ordersDTO;
        }
        public static Order[] ReadOrders(string fileName)
        {
            Order[] orders = ReadOrdersData(fileName).Select(orderData =>
            new Order(orderData.Id, orderData.MechanicId, orderData.CustomerId, orderData.ServiceList, orderData.IsFinished)).ToArray();

            return orders;
        }

        public static void WriteOrders(Order[] orders, string fileName)
        {
            string updatedJson = JsonSerializer.Serialize(orders, new JsonSerializerOptions { WriteIndented = true });
            File.WriteAllText(fileName, updatedJson);
        }

    }

}
