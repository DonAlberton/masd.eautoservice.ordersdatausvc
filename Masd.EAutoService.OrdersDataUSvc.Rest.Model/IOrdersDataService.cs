﻿namespace Masd.EAutoService.OrdersDataUSvc.Rest.Model
{
    public interface IOrdersDataService
    {
        public OrderDTO[] GetOrders();
        public OrderDTO GetOrder(int id);

        public void AddOrder(int mechanicId, int customerId, List<int> servicesList);

        public void EditOrder(int id, bool isFinished);
        public void AddServicesToOrder(int  id, List<int> services);
        public int GetOrderId();

    }
}
