﻿namespace Masd.EAutoService.OrdersDataUSvc.Rest.Model
{
    public class OrderDTO
    {
        public int Id { get; set; }
        public int MechanicId { get; set; }
        public int CustomerId { get; set; }
        public List<int> ServiceList { get; set; }
        public bool IsFinished { get; set; }

        public override string ToString()
        {
            return $"id: {Id}, mechanicId: {MechanicId}, customerId: {CustomerId}, " +
                $"serviceList: {ServiceList} , isFinished: {IsFinished}";
        }

    }
}