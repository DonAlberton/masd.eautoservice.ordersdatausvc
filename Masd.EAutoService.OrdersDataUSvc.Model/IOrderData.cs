﻿namespace Masd.EAutoService.OrdersDataUSvc.Model
{
    public interface IOrderData
    {
        public Order[] GetOrders();
        public Order GetOrder(int id);

        public void AddOrder(int mechanicId, int customerId, List<int> servicesList);

        public void EditOrder(int id, bool isFinished);

        public void AddServicesToOrder(int id, List<int> services);

        public int GetOrderId();
    }
}
