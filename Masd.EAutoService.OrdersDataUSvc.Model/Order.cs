﻿namespace Masd.EAutoService.OrdersDataUSvc.Model
{
    public class Order
    {
        public int Id { get; set; }
        public int MechanicId { get; set; }
        public int CustomerId { get; set; }
        public List<int> ServiceList { get; set; }
        public bool IsFinished { get; set; }

        public Order(int orderId, int mechanicId, int customerId, List<int> serviceList, bool isFinished)
        {
            Id = orderId;
            MechanicId = mechanicId;
            CustomerId = customerId;
            ServiceList = serviceList;
            IsFinished = isFinished;
        }
    }
}