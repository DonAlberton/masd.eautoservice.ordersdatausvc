FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src

EXPOSE 80

COPY . .

# Updates the dependencies of the entire solution
RUN dotnet restore

# Builds BlazorServer project
RUN dotnet build "Masd.EAutoService.OrdersDataUSvc.Rest" -c Debug -o /app/build

FROM build AS publish

RUN dotnet publish "Masd.EAutoService.OrdersDataUSvc.Rest/Masd.EAutoService.OrdersDataUSvc.Rest.csproj" -c Debug -o /app/publish


FROM base AS final
WORKDIR /app


COPY --from=publish /app/publish .

# Only used once in a Dockerfile
# RUN can be used multiple times
ENTRYPOINT ["dotnet", "Masd.EAutoService.OrdersDataUSvc.Rest.dll"]
