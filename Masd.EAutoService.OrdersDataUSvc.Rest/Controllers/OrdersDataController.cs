using Microsoft.AspNetCore.Mvc;
using Masd.EAutoService.OrdersDataUSvc.Logic;
using Masd.EAutoService.OrdersDataUSvc.Model;

namespace Masd.EAutoService.OrdersDataUSvc.Rest.Controllers
{
    [ApiController]
    [Route("[Controller]")]
    public class OrdersDataController : ControllerBase, IOrderData
    {
        private readonly ILogger<OrdersDataController> logger;
        private readonly IOrderData orderRepo;

        public OrdersDataController(ILogger<OrdersDataController> logger)
        {
            this.logger = logger;
            orderRepo = new OrderData();
        }

        [HttpGet]
        [Route("GetOrders")]
        public Order[] GetOrders()
        {
            return orderRepo.GetOrders();
        }

        [HttpGet]
        [Route("GetOrder")]
        public Order GetOrder(int id)
        {
            return orderRepo.GetOrder(id);
        }


        [HttpGet]
        [Route("GetOrderId")]
        public int GetOrderId()
        {
            return orderRepo.GetOrders().Last().Id;
        }


        [HttpPost]
        [Route("AddOrder")]
        public void AddOrder(int mechanicId, int customerId, List<int> servicesList)
        {
            orderRepo.AddOrder(mechanicId, customerId, servicesList);
        }



        [HttpPut]
        [Route("EditOrder")]
        public void EditOrder(int id, bool isFinished)
        {
            orderRepo.EditOrder(id, isFinished);
        }

        [HttpPut]
        [Route("AddServicesToOrder")]
        public void AddServicesToOrder(int id, List<int> services)
        {
            orderRepo.AddServicesToOrder(id, services);
        }
    }
}