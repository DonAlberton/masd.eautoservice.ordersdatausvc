﻿using Masd.EAutoService.OrdersDataUSvc.Rest.Model;

namespace Masd.EAutoService.OrdersDataUSvc.Rest.Client
{
    public class MockOrdersDataServiceClient : IOrdersDataService
    {

        public static OrderDTO[] ordersDTO = new OrderDTO[] {
            new OrderDTO { Id = 1, MechanicId = 1, CustomerId = 1, ServiceList = new List<int> {1, 2, 3 }  , IsFinished = true},
            new OrderDTO { Id = 2, MechanicId = 2, CustomerId = 2, ServiceList = new List<int> {1, 3}, IsFinished = true},
            new OrderDTO { Id = 3, MechanicId = 1, CustomerId = 3, ServiceList = new List<int> {1, 2, 4}, IsFinished = false} };
        public OrderDTO[] GetOrders()
        {
            return ordersDTO;
        }

        public OrderDTO GetOrder(int id)
        {
            return ordersDTO.FirstOrDefault(m => m.Id == id);
        }

        public int GetOrderId()
        {
            return ordersDTO.Last().Id;
        }


        public void AddOrder(int mechanicId, int customerId, List<int> servicesList)
        {
            OrderDTO order = new OrderDTO { Id = ordersDTO.Last().Id + 1, MechanicId = mechanicId, CustomerId = customerId, ServiceList = servicesList, IsFinished = false };

            List<OrderDTO> ordersList = new List<OrderDTO>(ordersDTO)
            {
                order
            };

            ordersDTO = ordersList.ToArray();
        }

        public void EditOrder(int id, bool isFinished)
        {

            ordersDTO.FirstOrDefault(m => m.Id == id).IsFinished = isFinished;
        }

        public void AddServicesToOrder(int id, List<int> services)
        {
            HashSet<int> existingServiceList = new HashSet<int>(ordersDTO.FirstOrDefault(m => m.Id == id).ServiceList);

            existingServiceList.UnionWith(new HashSet<int>(services));

            ordersDTO.FirstOrDefault(m => m.Id == id).ServiceList = existingServiceList.ToList();
        }
    }
}